<?php
// Pour le plugin Manuel de redaction du site https://contrib.spip.net/Manuel-de-redaction-du-site
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'diapo_q' => 'Hoe kun je een diashow in een artikel opnemen?',
	'diapo' => 'Nadat je afbeeldingen aan een artikel hebt toegevoegd, kan je een diashow aan je tekst toevoegen met <code><sjcycleXX></code> (XX is het artikelnummer). Er is een aantal opties beschikbaar, die je kunt zien door op de Help knop in de linkerkolom te drukken (wanneer je een artikel bewerkt)',
);
