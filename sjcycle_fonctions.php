<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Filtre pour un id unique dans les modeles
 * @param int $length
 * @return false|string
 */
function randomString($length = 8){
	$passe = "";
	$consonnes = array("b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "z", "bl", "br", "cl", "cr", "ch", "dr", "fl", "fr", "gl", "gr", "pl", "pr", "qu", "sl", "sr");
	$voyelles = array("a", "e", "i", "o", "u", "ae", "ai", "au", "eu", "ia", "io", "iu", "oa", "oi", "ou", "ua", "ue", "ui");

	$nbrC = count($consonnes) - 1;
	$nbrV = count($voyelles) - 1;

	for ($i = 0; $i < $length; $i++){
		$passe .= $consonnes[rand(0, $nbrC)] . $voyelles[rand(0, $nbrV)];
	}
	return substr($passe, 0, $length);
}

/**
 * Insertion dans le head des CSS
 * @param $flux
 * @return string
 */
function sjcycle_insert_head_css($flux) {
	include_spip('inc/config');
	$tooltip = lire_config('sjcycle/tooltip');

	if ($tooltip) {
		$flux .="\n".'<link rel="stylesheet" href="'.timestamp(find_in_path('lib/cycle2/jquery.tooltip.css')).'" type="text/css" media="all" />';
	}
	$flux .= "\n".'<link rel="stylesheet" type="text/css" href="'.timestamp(find_in_path('css/sjcycle.css')).'" media="all" />'."\n";

	return $flux;
}

/**
 * Insertion des JS de sjcycle
 * @param $flux
 * @return string
 */
function sjcycle_insert_head($flux) {
	include_spip('inc/config');
	$tooltip = lire_config('sjcycle/tooltip');

	foreach (array(
		'jquery.cycle2.js',
        'jquery.cycle2.flip.js',
		'jquery.cycle2.carousel.js',
		'jquery.cycle2.scrollVert.js',
		'jquery.cycle2.shuffle.js',
		'jquery.cycle2.tile.js'
     ) as $js) {
        if($path = find_in_path('lib/cycle2/'.$js)){
            $flux .="\n".'<script src="'.timestamp($path).'" type="text/javascript"></script>';
        }
	}

	if ($tooltip) {
        if($path = find_in_path('lib/cycle2/jquery.tooltip.js')){
            $flux .="\n".'<script src="'.timestamp($path).'" type="text/javascript" charset="utf-8"></script>';
        }
	}

	return $flux;
}

/**
 * Pipeline affiche gauche
 *
 * @param array $flux
 * @return mixed
 */
function sjcycle_affiche_gauche($flux){
	if ($flux['args']['exec'] == 'article_edit' and isset($flux['args']['id_article']) and (intval($flux['args']['id_article']) > 0)) {
		include_spip('inc/documents');
		include_spip('inc/config');
		$conf_sjcycle = lire_config('sjcycle');
		if($conf_sjcycle['afficher_aide']) {
			$document='';
			$document = sql_countsel('spip_documents as docs JOIN spip_documents_liens AS lien ON docs.id_document=lien.id_document', '(lien.id_objet='.$flux["args"]["id_article"].') AND (lien.objet="article") AND (docs.extension REGEXP "jpg|png|gif")');
			if ($document >= 2){
				$flux['data'] .= recuperer_fond('prive/navigation/bloc_aide', array('id_article' => $flux["args"]["id_article"]));
			}
		}
	 }
    return $flux;
}
